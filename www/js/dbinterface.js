
var dbName = "Test2";
var dbVersion = '1.0';
var dbPatch='1.2';
var dbDescription = "Vibram Test";
var dbSize = 5 * 1024 * 1024;
var db;
var lastSyncData;
var firstTimeSynchro=false;

function OpenDb() {
	if(debug) alert('OpenDb function called');
	if(!db) {
		if(debug) alert('Opening db');
		db = openDatabase(dbName,
						  dbVersion,
						  dbDescription,
						  dbSize
						 );
	} else {
		if(debug) alert('Db already opened');
	}
	if(debug) alert('OpenDb function ended');
}

function checkDbExistance(callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select existance');
								 tx.executeSql('SELECT version FROM existance', [], function(tx, results) { dbExistance(results, callback); }, function() { dbNotExists(callback); });
								},
				   errorDB);
}

function dbExistance(results, callback) {
	console.log('dbExistance');
    if(results.rows.length == 0) {
        console.log('dbNotExists');
		dbNotExists();
	}
	else {
        console.log('dbAlreadyExists');
		if ((results.rows.length==1)&&(results.rows.item(0).version!=dbPatch))
		{
            console.log("Updating DB");
			updateDb(db,results.rows.item(0).version,dbPatch,getUniqueUser,callback);
		}
		else
		{
			if(debug) alert("Db already exists");
			// Get the user present in user tableffffff
			getUniqueUser(callback);
		}
	}
}
/*
 function dbExistance(results, callback) {
 if(results.rows.length == 0) {
 dbNotExists();
 }
 else {
 if(debug) alert("Db already exists");
 // Get the user present in user table
 getUniqueUser(callback);
 }
 }
 */
function dbNotExists(callback) {
	OpenDb();
	db.transaction(generateDb, errorDB, callback);
}

function generateDb(tx) {
	if(debug) alert("generateDb function called");
	tx.executeSql('DROP TABLE IF EXISTS existance');
	tx.executeSql('CREATE TABLE IF NOT EXISTS existance (id unique, version)');
	tx.executeSql("INSERT INTO existance (id, version) VALUES (?, ?)", Array(0, dbPatch));
	tx.executeSql('DROP TABLE IF EXISTS users');
	tx.executeSql('CREATE TABLE IF NOT EXISTS users (id_utente, nome, cognome, email, password, hash, permessi, attivazione)');
	tx.executeSql('DROP TABLE IF EXISTS categorie');
	tx.executeSql('CREATE TABLE IF NOT EXISTS categorie (id_categorie, id_livello1, id_livello2, id_livello3, id_livello4, ordinamento_cat, codice_colore, status, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS categorie_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS categorie_contenuti (id_cat_contenuto, id_categoria, id_lingua, categoria_testo, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti (id_prodotto int, codice_prodotto, immagine, scala_misure, misure, misure_tecniche, dimensioni_foglio, allegato, numero_download, correlazione_automatica, nuovo, status, usa, usa_approvato, data_modifica, catalogo_exclusive,sample,eumarketonly)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_allegati');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_allegati (id_allegati, id_prodotto int, nome_allegato, tipo_allegato, estensione, numero_download, status, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_colori');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_colori (id_colori, codice_colore, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_colori_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_colori_contenuti (id_colori_contenuto, id_colori, id_lingua, testo_colori, data_modifica)');	
	tx.executeSql('DROP TABLE IF EXISTS prodotti_compound');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_compound (id_compound, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_compound_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_compound_contenuti (id_compound_contenuto, id_compound, id_lingua, testo_compound, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_construction');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_construction (id_construction, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_construction_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_construction_contenuti (id_construction_contenuto, id_construction, id_lingua, testo_construction, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_contenuti (id_contenuto, id_prodotto int, id_lingua, titolo, descrizione_repair, descrizione, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_country');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_country (id_country, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_country_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_country_contenuti (id_country_contenuto, id_country, id_lingua, testo_country, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_mescole');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_mescole (id_mescole, density, hardness, abrasion, elongation, stress, immagine , data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_mescole_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_mescole_contenuti (id_mescole_contenuto, id_mescole, id_lingua, testo_mescole, descrizione_mescole, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_categorie');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_categorie (id_prodotto int, id_categoria, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_colori');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_colori (id_prodotto int, id_colori, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_compound');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_compound (id_prodotto int, id_compound, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_construction');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_construction (id_prodotto int, id_construction, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_country');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_country (id_prodotto int, id_country, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_mescole');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_mescole (id_prodotto int, id_mescole, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_brand');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_brand (id_prodotto int, id_brand, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS synchronization');
	tx.executeSql('CREATE TABLE IF NOT EXISTS synchronization (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date, addedProducts, updatedProducts, maxDate)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_brand');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_brand (id_brand int, nome_brand, indirizzo_brand, status, data_modifica)');
    tx.executeSql('DROP TABLE IF EXISTS prodotti_correlati');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_correlati (id_prodotto_1 int, id_prodotto_2 int, data_modifica)');
	if(debug) alert("generateDb function ended");
}

function getUniqueUser(callback) {
	if(debug) alert("Function getUniqueUser called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT * FROM users', [], function(tx, results) {returnUser(results, function(user) {checkUserOnlineStatus(user, callback);});}, errorDB);
								},
				   errorDB);
}

function checkUserOnlineStatus(user, callback) {
	if(!user) {
		callback(user);
	}
	else {
		var networkState = navigator.onLine;
		if(debug2) alert("Connection type: " + networkState);
		if(networkState){
			checkOnlineStatus(user.email, user.password, user, callback);
		}
		else {
			callback(user);
		}
	}
}

function checkOnlineStatus(username, password, user, callback) {
	$.ajax({
			url: 'http://www.vibram.com/appcheck/index.php?v=1&email=' + username + '&code=' + password,
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				if(debug) alert(data);
				//attivo: "si"
				//dati_utente: Object
					//attivazione: "15-02-2012 11:46:03"
					//cognome: "Mischiatti"
					//id_utente: "1"
					//nome: "Luca"
					//permessi: "open"
					//__proto__: Object
					//__proto__: Object
				if(data.attivo == "nonpresente") {
					callback();
					return;
				}
				if(data.attivo == "no") {
					callback();
					return;
				}
				if(data.attivo == "si") {
					callback(user);
					return;
				}
			},
			error: function(error){
				alert(error);
			}
		});
}

function checkAuthentication(username, password, returnAuthentication) {
	if(debug) alert("Function checkAuthentication called");
	OpenDb();
	// 1. controllo se online
	var networkState = navigator.onLine;
	if(debug) alert("Connection type: " + networkState);
	if(networkState){
		// E' online -> controllo login su server remoto
		$.ajax({
			url: 'http://www.vibram.com/appcheck/index.php?v=1&email=' + username + '&code=' + password,
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				if(debug) alert(data);
				console.log(data)
				//attivo: "si"
				//dati_utente: Object
					//attivazione: "15-02-2012 11:46:03"
					//cognome: "Mischiatti"
					//id_utente: "1"
					//nome: "Luca"
					//permessi: "open"
					//__proto__: Object
					//__proto__: Object
				if(data.attivo == "nonpresente") {
					returnAuthentication(data.attivo);
					return;
				}
				if(data.attivo == "no") {
					returnAuthentication(data.attivo);
					return;
				}
				if(data.attivo == "si") {
                    window.localStorage.setItem("UserId",data.dati_utente.id_utente);
                    window.localStorage.setItem("Password",data.dati_utente.password);
					insertUserOnDb(data.dati_utente, username, password, function() { returnAuthentication(data.attivo, data.dati_utente.id_utente); });
					return;
				}
			},
			error: function(error){
				alert(error);
			}
		});
	} else {
		//if(debug) alert("local authentication");
		console.log('local authentication');
		OpenDb();
		db.transaction(function(tx) {
									 if(debug) alert('select user');
									 console.log('login offline');
									 tx.executeSql('SELECT * FROM users WHERE email = ? AND password = ?', [username, password], function(tx, results) {returnCheckAuthenticationOffline(results, returnAuthentication);}, errorDB);
									},
					   errorDB);
	}
}


function returnCheckAuthenticationOffline(results, callback)
{
	if(results.rows.length ==0) {
		console.log('no user');
		callback('nonpresente');
	} else {
		var item = results.rows.item(0);
		console.log(item.email);
		var user = new User(item.id_utente, item.nome, item.cognome, item.email, item.password, item.hash, item.permessi, item.attivazione);
		callback('si', user.id);
	}
}



function insertUserOnDb(user, username, password, callback) {
	if(debug) alert("Function insertUserOnDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT id_utente FROM users WHERE id_utente = ?', [user.id_utente], function(tx, results) {returnInsertUser(user, username, password, results, callback);}, errorDB);
								},
				   errorDB);
}


function deleteUserFromDb(callback)
{
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('DELETE FROM users', [], function(tx, results) {callback(true);}, errorDB);
								},
				   errorDB);
}



function returnInsertUser(user, username, password, results, callback) {
	if(debug) alert('User insert results:' + results.rows.length);
	if(results.rows.length == 0) {
		// l'utente non c'è e lo devo creare
		db.transaction(function(tx) {
								 if(debug) alert('insert user');
								 tx.executeSql("INSERT INTO users (id_utente, nome, cognome, email, password, permessi, attivazione) VALUES (?, ?, ?, ?, ?, ?, ?)", Array(user.id_utente, user.nome, user.cognome, username, password, user.permessi, user.attivazione), callback, errorDB);
								},
				   errorDB);
	}
	else {
		db.transaction(function(tx) {
								 if(debug) alert('insert user');
								 tx.executeSql("DELETE FROM users WHERE id_utente=?", Array(user.id_utente));
								 tx.executeSql("INSERT INTO users (id_utente, nome, cognome, email, password, permessi, attivazione) VALUES (?, ?, ?, ?, ?, ?, ?)", Array(user.id_utente, user.nome, user.cognome, username, password, user.permessi, user.attivazione), callback, errorDB);
								},
				   errorDB);
	}
}

/*
function synchronizeDb(userId, callback) {
	//alert("synchronizeDb function called");
	
	var networkState = navigator.onLine;
	if(debug2) alert("Connection type: " + networkState);
	if(networkState){
		 
		  var url = "http://www.vibram.com/appcheck/index.php?v=2&data="
		  if(lastSyncData) {
			  url += lastSyncData;
		  }
		  else {
			  url += "2012-01-01";
		  }
		  url += "&id=" + userId;
		  console.log('URL = ' + url);
		  $.ajax({
			url: url,
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				OpenDb();
				//alert('data.length = ' + data.length);
				if(data.length==1){// setto 1 perché la data c'è sempre!!!
					insertLastSync(callback);	
				}
				
				else {
						for (var i in data)
						{
							updates += i.length;	
						}
						updates += -1; //la lista degli aggiornamenti contiene anche la data dell'ultimo, quindi questa non viene contata.
						console.log('************** Numero di updates: ' + updates + '****************');
					
						console.log('chiamo dbInterface');
						db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
				}
				
			},
			error: function(error){
				alert("Error while downloading sync data");
			}
		  
		});
	}
	else {
		callback("NO");	// connection not available
	}
}
*/
function synchronizeDb(userId, callback) {
	//alert("synchronizeDb function called");
	
	var networkState = navigator.onLine;
	if(debug2) alert("Connection type: " + networkState);
	if(networkState){
		// E' online -> controllo login su server remoto
		/*$.getJSON("http://www.vibram.com/appcheck/index.php?",
         {
         v: 2,
         data: "2012-01-01",
         id: userId
         },
         function(data) {
         if(debug2) alert(data);
         OpenDb();
         db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
         });*/
        var url = "http://www.vibram.com/appcheck/index.php?v=2&data="
        if(lastSyncData) {
            url += lastSyncData;
        }
        else {
            console.log("setting local variable : first time download");
            window.localStorage.setItem("FirstTimeFiles","y");
            url += "2012-01-01";
        }
        
        console.log("Checkpoint Kilo: Leggo UserId="+UserId);
        if (window.localStorage.getItem("UserId"))
        {
            url += "&id="+window.localStorage.getItem("UserId");
        }
        else
        {
            url += "&id=1";
        }
        
        console.log('URL = ' + url);
        $.ajax({
               url: url,
               dataType: 'jsonp',
               jsonp: 'jsoncallback',
               timeout: 20000,
               success: function(data, status){
               OpenDb();
               //alert('data.length = ' + data.length);
               
               /*
                if(data.length==1){// setto 1 perché la data c'è sempre!!!
                insertLastSync(callback);
				}
				
				else {
                for (var i in data)
                {
                updates += i.length;
                }
                updates += -1; //la lista degli aggiornamenti contiene anche la data dell'ultimo, quindi questa non viene contata.
                console.log('************** Numero di updates: ' + updates + '****************');
                
                console.log('chiamo dbInterface');
                db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
				}
				*/
               
               
               db.transaction(function(tx) {
                              if(debug) alert('insert user');
                              tx.executeSql("select * from synchronization", [], function(tx,result){
                                            
                                            //callback
                                            if (result)
                                            {
                                            if(result.rows.length>0)
                                            {
                                            console.log("Non è la prima sync");
                                            console.log("Non è la prima sync");
                                            console.log("Non è la prima sync");
                                            console.log("Non è la prima sync");
                                            console.log("Non è la prima sync");
                                            firstTimeSynchro=false;
                                            }
                                            else
                                            {
                                            console.log("E' la prima sync");
                                            console.log("E' la prima sync");
                                            console.log("E' la prima sync");
                                            console.log("E' la prima sync");
                                            console.log("E' la prima sync");
                                            firstTimeSynchro=true;
                                            }
                                            }
                                            //alert('data.length = ' + data.length);
                                            if(data.length==1){// setto 1 perché la data c'è sempre!!!
                                            insertLastSync(callback);
                                            }
                                            
                                            else {
                                            for (var i in data)
                                            {
                                            updates += i.length;
                                            }
                                            updates += -1; //la lista degli aggiornamenti contiene anche la data dell'ultimo, quindi questa non viene contata.
                                            console.log('************** Numero di updates: ' + updates + '****************');
                                            
                                            console.log('chiamo dbInterface');
                                            db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
                                            }
                                            
                                            
                                            
                                            
                                            
                                            
                                            }, errorDB); //VERIFICARE maxDate
                              },
                              errorDB);
               
               },
               error: function(error){
               alert("Error while downloading sync data");
               }
               
               });
	}
	else {
		callback("NO");	// connection not available
	}
}

var totalrows = 0;
var ended = false;
var imagesToDownload = Array();
var smallImagesToDownload = Array();
var pdfToDownload = Array();
var dataUltimoAggiornamento = '';
var updates = 0;

function populateDb(tx, data, callback) {
	//console.log(data.length);
	//if(data.length == 0) {
	//	insertLastSync(callback);
	//}	
	dataUltimoAggiornamento = data[0].data_max;
	console.log('Data ultimo aggiornamento = ' + dataUltimoAggiornamento);
	for(tableIndex in data) {
		var table = data[tableIndex];
		var nomeTabella = table.nome_tabella;
		//alert("tabella: " + table.nome_tabella);
		for(rowIndex in table.data) {
			//console.log("nome tabella processata = "+table.nome_tabella)
			var row = table.data[rowIndex];
			var idName = null;
			var idName2 = null;
			var parameters = Array();
			if(table.nome_tabella == 'categorie') idName = 'id_categorie';
			else if(table.nome_tabella == 'categorie_contenuti') idName = 'id_cat_contenuto';
			else if(table.nome_tabella == 'prodotti') idName = 'id_prodotto';
			else if(table.nome_tabella == 'prodotti_allegati') idName = 'id_allegati';
			else if(table.nome_tabella == 'prodotti_colori') idName = 'id_colori';
			else if(table.nome_tabella == 'prodotti_colori_contenuti') idName = 'id_colori_contenuto';
			else if(table.nome_tabella == 'prodotti_compound') idName = 'id_compound';
			else if(table.nome_tabella == 'prodotti_compound_contenuti') idName = 'id_compound_contenuto';
			else if(table.nome_tabella == 'prodotti_construction') idName = 'id_construction';
			else if(table.nome_tabella == 'prodotti_construction_contenuti') idName = 'id_construction_contenuto';
			else if(table.nome_tabella == 'prodotti_contenuti') idName = 'id_contenuto';
			else if(table.nome_tabella == 'prodotti_country') idName = 'id_country';
			else if(table.nome_tabella == 'prodotti_country_contenuti') idName = 'id_country_contenuto';
			else if(table.nome_tabella == 'prodotti_mescole') idName = 'id_mescole';
			else if(table.nome_tabella == 'prodotti_mescole_contenuti') idName = 'id_mescole_contenuto';
			else if(table.nome_tabella == 'prodotti_brand') idName = 'id_brand';
			else if(table.nome_tabella == 'relazione_prodotti_categorie') {
				idName = 'id_prodotto';
				idName2 = 'id_categoria';
			}
			else if(table.nome_tabella == ' relazione_prodotti_colori') {
				idName = 'id_prodotto';
				idName2 = 'id_colori';
			}
			else if(table.nome_tabella == 'relazione_prodotti_compound') {
				idName = 'id_prodotto';
				idName2 = 'id_compound';
			}
			else if(table.nome_tabella == 'relazione_prodotti_construction') {
				idName = 'id_prodotto';
				idName2 = 'id_construction';
			}
			else if(table.nome_tabella == 'relazione_prodotti_country') {
				idName = 'id_prodotto';
				idName2 = 'id_country';
			}
			else if(table.nome_tabella == 'relazione_prodotti_mescole') {
				idName = 'id_prodotto';
				idName2 = 'id_mescole';
			}
			else if(table.nome_tabella == 'relazione_prodotti_brand') {
				idName = 'id_prodotto';
				idName2 = 'id_brand';
			}
            else if(table.nome_tabella == 'prodotti_correlati') {
				idName = 'id_prodotto_1';
				idName2 = 'id_prodotto_2';
			}
            //duplicati prodotti_correlati
            
			parameters.push(row[idName]);
			var sqlString = 'SELECT * FROM ' + table.nome_tabella + ' WHERE ' + idName + ' = ?';
			if(idName2) {
				sqlString += ' AND ' + idName2 + ' = ?';
				parameters.push(row[idName2]);
				//if(table.nome_tabella != 'relazione_prodotti_categorie' && (idName == 'id_categoria' || idName2 == 'id_categoria')) alert(sqlString);
			}
			totalrows++;
            //correzione duplicati delete singla chiave
			executesql(tx,sqlString, parameters, row, idName, nomeTabella, callback,idName2);
            
            //diagnostica 2
            /*
             if(table.nome_tabella == 'prodotti_correlati') {
             console.log(sqlString+" "+parameters);
             }
             */
            
		}
	}
	ended = true;
}
/*
 parameters.push(row[idName]);
 var sqlString = 'SELECT * FROM ' + table.nome_tabella + ' WHERE ' + idName + ' = ?';
 if(idName2) {
 sqlString += ' AND ' + idName2 + ' = ?';
 parameters.push(row[idName2]);
 //if(table.nome_tabella != 'relazione_prodotti_categorie' && (idName == 'id_categoria' || idName2 == 'id_categoria')) alert(sqlString);
 }
 totalrows++;
 executesql(tx,sqlString, parameters, row, idName, nomeTabella, callback);
 }
 }
 ended = true;
 }
 */



function getLastSyncToCheckUpdates(callback)
{
	
	var networkState = navigator.onLine;
	if(networkState)
	{
		OpenDb();
		
		db.transaction(function(tx) {
								 console.log('select sync short detail');
								 var sqlQuery = "SELECT * FROM synchronization WHERE addedProducts != 0 OR updatedProducts != 0 ORDER BY maxDate DESC";
								 tx.executeSql(sqlQuery,[], function(tx, results) { getLastSyncToCheckUpdatesResult(callback, results) }, errorDB);
								},
				   errorDB);
	}
	
	else callback(false);
}

function getLastSyncToCheckUpdatesResult(callback, results)
{
	var url = "http://www.vibram.com/appcheck/index.php?v=2&data="
	//alert(results.rows.item(0).maxDate)
	if(results.rows.length == 0) lastSyncData = '2012-01-01';
	else lastSyncData = results.rows.item(0).maxDate;
	console.log('LAST SYNCHRONIZATION DATA: ' + lastSyncData);
	url += lastSyncData; 
	
    if (window.localStorage.getItem("UserId"))
    {
        url += "&id="+window.localStorage.getItem("UserId");
    }
    else
    {
        url += "&id=1";
    }
    
	console.log('REQUEST TO URL: ' + url);
	
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonp: 'jsoncallback',
		timeout: 20000,
		success: function(data, status){
			console.log("questi i risultati degli aggiornamenti - - - - - -- - - - - - - - - "+data )
			console.log("questi i risultati degli aggiornamenti - - - - - -- - - - - - - - - "+data.length )
			if(data.length>1) {
				callback(true);
			} else {
				callback(false);
			}
		}
	});
}


/*
 function executesql(tx,sqlString, parameters, row, idName, nomeTabella, callback) {
 tx.executeSql(sqlString, parameters, function(tx, results) {selectId(tx, results, row, idName, nomeTabella, callback);});
 }*/
function executesql(tx,sqlString, parameters, row, idName, nomeTabella, callback,idName2) {
    //console.log("inside executesql");
	tx.executeSql(sqlString, parameters, function(tx, results) {selectId(tx, results, row, idName, nomeTabella, callback,idName2);});
}
/*
 function selectId(tx, results, row, idName, nome_tabella, callback) {
 var properties = new Array();
 var parameters = new Array();
 var questionMarks = new Array();
 for(property in row) {
 properties.push(property);
 parameters.push(row[property]);
 questionMarks.push('?');
 if(nome_tabella == 'prodotti' && property == 'immagine') {
 var imgFileName = row[property];
 if(imgFileName.length > 0) {
 imagesToDownload.push(imgFileName);
 smallImagesToDownload.push(imgFileName);
 }
 }
 else if(nome_tabella == 'prodotti' && property == 'allegato') {
 if(row[property].length > 0) {
 pdfToDownload.push(row[property]);
 }
 }
 else if (nome_tabella == 'prodotti_allegati' && property == 'nome_allegato') {
 if(row['tipo_allegato'] == 'file') {
 pdfToDownload.push(row[property]);
 }
 else if(row['tipo_allegato'] == 'immagine') {
 var imgFileName = row[property];
 if(imgFileName.length > 0) {
 imagesToDownload.push(imgFileName);
 smallImagesToDownload.push(imgFileName);
 }
 }
 }
 }
 var sqlString = "";
 if(results.rows.length == 0)
 {
 sqlString = 'INSERT INTO ' + nome_tabella + ' (' + properties.join(',') + ') VALUES (' + questionMarks.join(',') + ')';
 }
 
 
 else {
 sqlString = 'DELETE FROM ' +nome_tabella + ' WHERE ' + idName + ' = ?';
 tx.executeSql(sqlString, new Array(row[idName]));
 sqlString = 'INSERT INTO ' + nome_tabella + ' (' + properties.join(',') + ') VALUES (' + questionMarks.join(',') + ')';
 
 }
 tx.executeSql(sqlString, parameters);
 
 totalrows--;
 if(ended == true && totalrows == 0) {
 //insertLastSync(function() {getFileSystem(callback);});
 getFileSystem(callback);
 }
 }*/

function selectId(tx, results, row, idName, nome_tabella, callback,idName2) {
    //console.log("inside selectID");
	var properties = new Array();
	var parameters = new Array();
	var questionMarks = new Array();
	for(property in row) {
		properties.push(property);
		parameters.push(row[property]);
		questionMarks.push('?');
		if(nome_tabella == 'prodotti' && property == 'immagine') {
			var imgFileName = row[property];
			if(imgFileName.length > 0) {
				imagesToDownload.push(imgFileName);
				smallImagesToDownload.push(imgFileName);
			}
		}
		else if(nome_tabella == 'prodotti' && property == 'allegato') {
			if(row[property].length > 0) {
				pdfToDownload.push(row[property]);
			}
		}
		else if (nome_tabella == 'prodotti_allegati' && property == 'nome_allegato') {
			if(row['tipo_allegato'] == 'file') {
				pdfToDownload.push(row[property]);
			}
			else if(row['tipo_allegato'] == 'immagine') {
				var imgFileName = row[property];
				if(imgFileName.length > 0) {
					imagesToDownload.push(imgFileName);
					smallImagesToDownload.push(imgFileName);
				}
			}
		}
	}
	var sqlString = "";
	if(results.rows.length == 0)
	{
		sqlString = 'INSERT INTO ' + nome_tabella + ' (' + properties.join(',') + ') VALUES (' + questionMarks.join(',') + ')';
        
        //diagnostica 3
        /*
         if(nome_tabella == 'prodotti_correlati') {
         console.log(sqlString+" "+parameters);
         }
         */
        //fine diagnostica 3
	}
	
	
	else {
		sqlString = 'DELETE FROM ' +nome_tabella + ' WHERE ' + idName + ' = ?';
        if(idName2) {
            
            sqlString += ' AND ' + idName2 + ' = ?';
            tx.executeSql(sqlString, new Array(row[idName],row[idName2]));
            //console.log("idName: "+idName+" idName2: "+idName2+" row[idName]: "+row[idName]+" row[idName2]: "+row[idName2]+" sqlstring: "+sqlString);
        }
        else
        {
            tx.executeSql(sqlString, new Array(row[idName]));
        }
        //diagnostica 3
        /*
         if(nome_tabella == 'prodotti_correlati') {
         console.log(sqlString+" "+parameters);
         }
         */
        //fine diagnostica 3
		sqlString = 'INSERT INTO ' + nome_tabella + ' (' + properties.join(',') + ') VALUES (' + questionMarks.join(',') + ')';
        
        //diagnostica 3
        /*
         if(nome_tabella == 'prodotti_correlati') {
         console.log(sqlString+" "+parameters);
         }
         */
        //fine diagnostica 3
		
	}
    
	tx.executeSql(sqlString, parameters);
	
	totalrows--;
	if(ended == true && totalrows == 0) {
		//insertLastSync(function() {getFileSystem(callback);});
		getFileSystem(callback);
	}
}

function getFileSystem(callback) {
	console.log('getFileSystem function called');
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                             
                             
                             //VIBRAMCATALOGUEOFFLINE E' LA NUOVA SOTTODIRECTORY PER IL SALVATAGGIO
                             console.log('Checkpoint Alpha-Sierra-Sierra: prima di fileSystem.root.getDirectory principale per il salvataggio dati');
                             
                             fileSystem.root.getDirectory("VibramCatalogueOffline", {create: true, exclusive: false},
                                                          function(parent) {
                                                          console.log('Checkpoint Sierra-Hotel-India-Tango:Inside VibramCatalogueOffline');
                                                          
                                                          
                             
                             
																			console.log('lancio la funzione downloadImage');
																			downloadImage(fileSystem, callback);
                             
                             
																		}, fail);
                             																		}, fail);
}


var updatedImages = 0;
var addedImages = 0;
var totalImages = 0;
var totalImageSmall = 0;
var totalPdf = 0;
var remainingImages;
var remainingImagesSmall;
var remainingPdf;
var errorImages = 0;
var errorImagesSmall = 0;
var errorPdf = 0;
var downloadFinished = 0;
var remainingFilesToDownload = 0;

function downloadImage(fileSystem, callback) {
	//alert(fileSystem.name);
	//alert(imgFileName);
	console.log('downlodImage function called');
	addedImages = imagesToDownload.length + smallImagesToDownload.length + pdfToDownload.length;
	remainingImages = imagesToDownload.length;
	remainingImagesSmall = smallImagesToDownload.length;
	remainingPdf = pdfToDownload.length;
	totalImages = imagesToDownload.length;
	totalImagesSmall = smallImagesToDownload.length;
	totalPdf = pdfToDownload.length;
	remainingFilesToDownload = remainingImages + remainingImagesSmall + remainingPdf;
	
	//VIBRAMCATALOGUEOFFLINE E' LA NUOVA SOTTODIRECTORY PER IL SALVATAGGIO
	fileSystem.root.getDirectory("VibramCatalogueOffline/images", {create: true, exclusive: false},
							function(parent) {
										console.log('Launch saveImage function');
										saveImage(parent, callback);
										
							}, fail);
	
	fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: true, exclusive: false},
							function(parent) {
										console.log('Launch saveImageSmall function');
										saveImageSmall(parent, callback);
							}, fail);
	fileSystem.root.getDirectory("VibramCatalogueOffline/pdf", {create: true, exclusive: false},
							function(parent) {
										console.log('Launch savePdf function');
										savePdf(parent, callback);
							}, fail);
}

function saveImage(parent, callback) {
	
	//fileSystem.root.getFile("7679_dupla.jpg", {create: false}, function(){console.log("il file 7679_dupla.jpg esiste");}, function(){console.log ("il file 7679_dupla.jpg non esiste");});
	//console.log('Funzione saveImage: Differenza remainingImages - errorImages = ' + (remainingImages - errorImages) + ', Differenza remainingPdf - errorPdf = ' + (remainingPdf - errorPdf));
	//console.log('Funzione saveImage: smallImagesToDownload.length = ' + smallImagesToDownload.length + ', pdfToDownload.length = ' + pdfToDownload.length);
	var fileTransfer = new FileTransfer(parent);
	//console.log('Creato l\'oggetto FileTransfer')
	//for (imgIndex in imagesToDownload) {
	if(imagesToDownload.length > 0) {
		var imgFileName = imagesToDownload.pop();
		//console.log('attempting to download ' + "http://www.vibram.com/vibramrepair/immagini_caricate/" + imgFileName);
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/immagini_caricate/" + imgFileName,
			parent.fullPath + "/" + imgFileName,
			function(entry) {
				//console.log("download complete: " + entry.fullPath);
				downloadImageSuccess(parent, callback);
			},
			function(error) {
				/*alert("download error source " + error.source);
				alert("download error target " + error.target);
				alert("upload error code" + error.code);*/
				downloadImageError(parent, callback);
			},
			imgFileName
		);
	
	}
	else //if(remainingImagesSmall - errorImagesSmall == -1 && remainingPdf - errorPdf == -1) {
	{		
		downloadFinished++;
		if(pdfToDownload.length == 0 && smallImagesToDownload.length == 0) {
			console.log('endimg');
			
			console.log('downloadFinished: ' + downloadFinished);
			if(downloadFinished == 3) insertLastSync(callback);
		}
	}
}

function downloadImageSuccess(parent, callback) {
	//$("#download_ok").html(remainingImages--);
	console.log('downloadImageSuccess called')
	remainingImages--;
	//console.log('Scaricate ' + imagesToDownload.length + ' immagini, ne rimangono ' + remainingImages);
	updateBarraLoading();
	saveImage(parent, callback);
}

function downloadImageError(parent, callback) {
	//$("#download_small_error").html(++errorImages);
	console.log('downloadImageError called')
	++errorImages;
	updateBarraLoading();
	saveImage(parent, callback);
}

function saveImageSmall(parent, callback) {
	//alert(parent.name);
	//console.log('Funzione saveImageSmall: Differenza remainingImages - errorImages = ' + (remainingImages - errorImages) + ', Differenza remainingPdf - errorPdf = ' + (remainingPdf - errorPdf));
	//console.log('Funzione saveImageSmall: imagesToDownload.length = ' + imagesToDownload.length + ', pdfToDownload = ' + pdfToDownload.length);
	var fileTransfer = new FileTransfer(parent);
	//console.log('Creato l\'oggetto FileTransfer')
	if(smallImagesToDownload.length > 0) {
		var imgFileName = smallImagesToDownload.pop();
		//console.log('attempting to download ' + "http://www.vibram.com/vibramrepair/immagini_caricate/piccole/" + imgFileName);
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/immagini_caricate/piccole/" + imgFileName,
			parent.fullPath + "/" + imgFileName,
			function(entry) {
				//console.log("download complete: " + entry.fullPath);
				downloadImageSmallSuccess(parent, callback);
			},
			function(error) {
				//console.log("download error source " + error.source);
				//console.log("download error target " + error.target);
				//console.log("upload error code" + error.code);
				downloadImageSmallError(parent, callback);
			},
			imgFileName
		);
	}
	
	else 
	{
		downloadFinished++;
		if(imagesToDownload.length == 0 && pdfToDownload.length == 0) {
		
			console.log('endsmall');
			
			console.log('downloadFinished: ' + downloadFinished);
			if(downloadFinished == 3) insertLastSync(callback);
		}
	}
}

function downloadImageSmallSuccess(parent, callback) {
	//$("#download_small_ok").html(remainingImagesSmall--);
	console.log('downloadImageSmallSuccess called')
	remainingImagesSmall--;
	//console.log('Scaricate ' + imagesToDownload.length + ' immagini piccole, ne rimangono ' + remainingImagesSmall);
	updateBarraLoading();
	saveImageSmall(parent, callback);
}

function downloadImageSmallError(parent, callback) {
	//$("#download_small_error").html(++errorImagesSmall);
	console.log('downloadImageSmallError called')
	++errorImagesSmall;
	updateBarraLoading();	
	saveImageSmall(parent, callback);
}

function savePdf(parent, callback) {
	//alert(parent.name);
	//console.log('Funzione savePdf called');
	//console.log('Funzione savePdf: Differenza remainingImages - errorImages = ' + (remainingImages - errorImages) + ', Differenza remainingImagesSmall - errorImagesSmall = ' + (remainingImagesSmall - errorImagesSmall));
	//console.log('Funzione savePdf: imagesToDownload.length = ' + imagesToDownload.length + ', smallImagesToDownload.length = ' + smallImagesToDownload.length);
	var fileTransfer = new FileTransfer(parent);
	//console.log('Creato l\'oggetto FileTransfer')
	if(pdfToDownload.length > 0) {
		var pdfFileName = pdfToDownload.pop();
		//console.log('attempting to download ' + "http://www.vibram.com/vibramrepair/scarica.php?code=" + pdfFileName + "&catalog=si");
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/scarica.php?code=" + pdfFileName + "&catalog=si",
			parent.fullPath + "/" + pdfFileName,
			function(entry) {
				//console.log("download complete: " + entry.fullPath);
				downloadPdfSuccess(parent, callback);
			},
			function(error) {
				//console.log("download error source " + error.source);
				//console.log("download error target " + error.target);
				//console.log("upload error code" + error.code);
				downloadPdfError(parent, callback);
			},
			pdfFileName
		);
	}
	
	else //if(remainingImages - errorImages == 0 && remainingImagesSmall - errorImagesSmall == 0) {
		{
			downloadFinished++;
			if(imagesToDownload.length == 0 && smallImagesToDownload.length == 0) {
				console.log('endpdf');
				
				console.log('downloadFinished: ' + downloadFinished);
				if(downloadFinished == 3) insertLastSync(callback);
			}
	}
}

function downloadPdfSuccess(parent, callback) {
	//$("#download_pdf_ok").html(remainingPdf--);
	console.log('downloadPdfSuccess called')
	remainingPdf--;
	console.log('Scaricati ' + (totalPdf - remainingPdf) + ' pdf, ne rimangono ' + remainingPdf);
	updateBarraLoading();
	savePdf(parent, callback);
}

function downloadPdfError(parent, callback) {
	//$("#download_pdf_error").html(++errorPdf);
	console.log('downloadPdfError called')
	++errorPdf;

	updateBarraLoading();
	savePdf(parent, callback);
}

function updateBarraLoading(){
	downloadedFiles = addedImages - (remainingImages + remainingImagesSmall + remainingPdf);
	downloadedPercent = parseInt((100 * downloadedFiles)/addedImages);
	barraLoadingNewWidth = (downloadedFiles * 100)/addedImages;
	$("#barra_loading").width(barraLoadingNewWidth);
	$("#valore_numerico").html(downloadedPercent + '%');
}

function insertLastSync(callback) {
	downloadFinished = 0;
	console.log
	db.transaction(function(tx) {
								 if(debug) alert('insert user');
								 tx.executeSql("INSERT INTO synchronization (date, addedProducts, updatedProducts, maxDate) VALUES (datetime('now','localtime'), ?, ?, ?)", Array(0, updates, dataUltimoAggiornamento), function(){getSyncFromDb(callback);}, errorDB); //VERIFICARE maxDate
								},
				   errorDB);
}

function getSynchronizationHistoryFromDb(callback){
	console.log('getSynchronizationHistoryFromDb');
	OpenDb();
	db.transaction(function(tx){
		tx.executeSql('SELECT * FROM synchronization ORDER BY date DESC', [], function(tx, results){getSynchronizationHistoryResult(results, callback);}, errorDB);
	}, 
	errorDB);
}

//tx.executeSql('CREATE TABLE IF NOT EXISTS synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)');

function getSynchronizationHistoryResult(results, callback){
	console.log('getSynchronizationHistoryResult');
	if(results.rows.length == 0){
		console.log('Never synchronized');	
	}
	else{
		var syncros = new Array();
		 
		//for(var synchro in results.rows){
			
		for(i=0; i<results.rows.length; i++) {
			console.log('Sincronizzazione id ' + results.rows.item(i).id + ' data ' + results.rows.item(i).date +' prodotti aggiunti ' + results.rows.item(i).addedProducts + ' prodotti aggiornati ' + results.rows.item(i).updatedProducts);
			syncros.push(new Sync(results.rows.item(i).id, results.rows.item(i).date, results.rows.item(i).addedProducts, results.rows.item(i).updatedProducts));
			
		}
		
		callback(syncros);
		
	}
	
}

function getUserFromDb(userId, callback) {
	console.log(userId);
	if(debug) alert("Function getUser called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT * FROM users WHERE id_utente = ?', [userId], function(tx, results) {returnUser(results, callback);}, errorDB);
								},
				   errorDB);
}

function returnUser(results, callback) {
	if(debug) alert('User results:' + results.rows.length);
	if(results.rows.length ==0) {
		console.log('no user');
		callback();
	} else {
		var item = results.rows.item(0);
		console.log(item.email);
		var user = new User(item.id_utente, item.nome, item.cognome, item.email, item.password, item.hash, item.permessi, item.attivazione);
		callback(user);
	}
}

function getCategoriesFromDb(callback) {
	if(debug) alert("Function getCategoriesFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select categories');
								 tx.executeSql('SELECT id_categorie, id_livello1, id_livello2, id_livello3, id_livello4, ordinamento_cat, status, categoria_testo, id_lingua FROM categorie JOIN categorie_contenuti ON categorie.id_categorie = categorie_contenuti.id_categoria WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCategories(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCategories(results, callback) {
	if(debug) alert('Categories results:' + results.rows.length);
	var allCategories = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).categoria_testo);
		allCategories.push(new Category(results.rows.item(i).id_categorie, results.rows.item(i).id_livello1, results.rows.item(i).id_livello2, results.rows.item(i).id_livello3, results.rows.item(i).id_livello4, results.rows.item(i).ordinamento_cat, results.rows.item(i).categoria_testo));
	}
	callback(allCategories);
}
//brand luca
function getBrandsFromDb(callback) {
	console.log("sono dentro alla funzione db brand")
	if(debug) alert("Function getBrandsFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select brand');
								 tx.executeSql('SELECT id_brand, nome_brand, indirizzo_brand FROM prodotti_brand ORDER BY nome_brand', [], function(tx, results) { exctractBrands(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractBrands(results, callback) {
	console.log("sono dentro alla callback db brand")
	if(debug) alert('Brands results:' + results.rows.length);
	var allBrands = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).nome_brand);
		allBrands.push(new Brand(results.rows.item(i).id_brand, results.rows.item(i).nome_brand, results.rows.item(i).indirizzo_brand));
	}
	callback(allBrands);
}

function getCompoundsFromDb(callback) {
	if(debug) alert("Function getCompoundFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select compounds');
								 tx.executeSql('SELECT prodotti_compound.id_compound, ordinamento, testo_compound FROM prodotti_compound JOIN prodotti_compound_contenuti ON prodotti_compound.id_compound = prodotti_compound_contenuti.id_compound WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCompounds(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCompounds(results, callback) {
	if(debug) alert('Compounds results:' + results.rows.length);
	var allCompounds = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).testo_compound);
		allCompounds.push(new Compound(results.rows.item(i).id_compound, results.rows.item(i).ordinamento, results.rows.item(i).testo_compound));
	}
	callback(allCompounds);
}

function getCountriesFromDb(callback) {
	if(debug) alert("Function getCountriesFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select countries');
								 tx.executeSql('SELECT prodotti_country.id_country, ordinamento, testo_country FROM prodotti_country JOIN prodotti_country_contenuti on prodotti_country.id_country = prodotti_country_contenuti.id_country WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCountries(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCountries(results, callback) {
	if(debug) alert('Countries results:' + results.rows.length);
	var allCountries = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).testo_country);
		allCountries.push(new Country(results.rows.item(i).id_country, results.rows.item(i).ordinamento, results.rows.item(i).testo_country));
	}
	callback(allCountries);
}


function getConstructionsFromDb(callback) {
	if(debug) alert("Function getConstructionsFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select constructions');
								 tx.executeSql('SELECT prodotti_construction.id_construction, ordinamento, testo_construction FROM prodotti_construction JOIN prodotti_construction_contenuti on prodotti_construction.id_construction = prodotti_construction_contenuti.id_construction WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractConstructions(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractConstructions(results, callback) {
	if(debug) alert('Constructions results:' + results.rows.length);
	var allConstructions = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).testo_construction);
		allConstructions.push(new Construction(results.rows.item(i).id_construction, results.rows.item(i).ordinamento, results.rows.item(i).testo_construction));
	}
	callback(allConstructions);
}

function getProductListFromDb(filters, sortOrder, order, start, count, exclusive, searchString, callback) {
	if(exclusive)  {
		aggiunta_exclusive=" AND prodotti.catalogo_exclusive='si'"	
	} else {
		aggiunta_exclusive=" AND prodotti.catalogo_exclusive='no'"
	}
	if(debug) alert("Function getProductListFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 console.log("------------------------------------------------------------------")
								 if(debug) alert('select products');
								 var parameters = new Array();
								 var sqlQuery = "SELECT prodotti.id_prodotto, codice_prodotto, immagine, nuovo, titolo, relazione_prodotti_country.id_country FROM prodotti " + 
								 				"LEFT JOIN relazione_prodotti_categorie ON prodotti.id_prodotto = relazione_prodotti_categorie.id_prodotto " +
												"LEFT JOIN relazione_prodotti_construction ON prodotti.id_prodotto = relazione_prodotti_construction.id_prodotto " +
												"LEFT JOIN relazione_prodotti_compound ON prodotti.id_prodotto = relazione_prodotti_compound.id_prodotto " +
												"LEFT JOIN relazione_prodotti_country ON prodotti.id_prodotto = relazione_prodotti_country.id_prodotto " +
												"LEFT JOIN relazione_prodotti_brand ON prodotti.id_prodotto = relazione_prodotti_brand.id_prodotto " +
												"LEFT JOIN prodotti_contenuti ON prodotti.id_prodotto = prodotti_contenuti.id_prodotto " +
												"LEFT JOIN categorie ON relazione_prodotti_categorie.id_categoria = categorie.id_categorie " +
												"WHERE prodotti.status = 'si' AND (usa = 'si' OR usa = 'no') AND (usa_approvato = 'si' OR usa_approvato = 'no') AND id_lingua = '2'";
								sqlQuery+=aggiunta_exclusive
												
								 for(filterIndex in filters) {
									var filter = filters[filterIndex];
									if(filter.filterValues.length > 0) {
										if(filter.filterName == 'category') {
											sqlQuery += " AND (id_categoria in ('";
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "') OR id_livello2 in ('";
											
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "'))";
										}
										else if(filter.filterName == 'construction') {
											sqlQuery += " AND id_construction in ('";
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "')";
										}
										else if(filter.filterName == 'compound') {
											sqlQuery += " AND id_compound in ('";
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "')";
										}
										else if(filter.filterName == 'country') {
											sqlQuery += " AND id_country in ('";
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "')";
										}
										else if(filter.filterName == 'brand') {
											sqlQuery += " AND id_brand in ('";
											sqlQuery += filter.filterValues.join("','");
											sqlQuery += "')";
										}
                                        else if(filter.filterName == 'USMARKETONLY')
                                        {
                   //aggiunta flag US MARKET
                   //per ora finchè luca non crea il flag uso una condizione a caso per testare
                                        sqlQuery += " AND prodotti.usa='si'";
                                        }
                                        else if(filter.filterName == 'SAMPLEONLY')
                                        {
                                        //aggiunta flag SAMPLE
                                        sqlQuery += " AND prodotti.sample='si'";
                                        }
                   else if(filter.filterName == 'EUMARKETONLY')
                   {
                   //aggiunta flag SAMPLE
                   sqlQuery += " AND eumarketonly='si'";
                   }
									}
								 }
								 if(searchString && searchString.length > 0) {
									 sqlQuery += " AND (codice_prodotto like ? OR titolo like ?)";
									 parameters.push(searchString + "%");
									 parameters.push(searchString + "%");
								 }
								 sqlQuery += ' GROUP BY prodotti.id_prodotto';
								 if(sortOrder == 'titolo') {
									 sqlQuery += ' ORDER BY prodotti_contenuti.' + sortOrder + ' ' + order;
								 } else {
									 sqlQuery += ' ORDER BY prodotti.' + sortOrder + ' ' + order;
								 }
								 console.log("la query >> " + sqlQuery)
								 console.log("------------------------------------------------------------------")
								 tx.executeSql(sqlQuery, parameters, function(tx, results) { exctractProductLists(results, filters, start, count, callback,searchString); }, errorDB);
								},
				   errorDB);
}
/*
function exctractProductLists(results, filters, start, count, callback) {
	if(debug) alert('Products results:' + results.rows.length);
	var allProducts = new Array();
	var limit = Math.min(start + count, results.rows.length);
	for(i=start; i<limit; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(results.rows.item(i).categoria_testo);
		allProducts.push(new Product(item.id_prodotto, item.codice_prodotto, item.titolo, null, null, item.id_country, null, item.nuovo, item.immagine, null, null, null, null, null, null, null, null));
	}
	callback(allProducts, results.rows.length);
}
*/

function getInfoProdottiCorrelatiFromDb(id_search,callback)
{
    /*
    db.transaction(function(tx) {
                   
                   
                   tx.executeSql('select * from prodotti where id_prodotto in (select id_prodotto_2 from prodotti_correlati where id_prodotto_1 = '+id_search+')', [], function(tx, results) {
                                 
                                 if (results)
                                 {
                                 var conto=results.rows.length;
                                 if (conto==0)
                                 {
                                 console.log("Checkpoint: inside getInfoProdottiCorrelatiFromDb zero results branch");
                                 var queryC='SELECT * FROM (select * from prodotti as p where p.id_prodotto!=\''+id_search+'\' and p.status=\'si\' ) as p join (select distinct rpc.id_prodotto from relazione_prodotti_categorie as rpc where rpc.id_categoria in (SELECT distinct cc.id_categoria FROM relazione_prodotti_categorie AS rpc join categorie_contenuti AS cc on rpc.id_categoria=cc.id_categoria WHERE rpc.id_prodotto=\''+id_search+'\'          ) and rpc.id_prodotto!=\''+id_search+'\' ) AS rpc on p.id_prodotto=rpc.id_prodotto join (select distinct rpco.id_prodotto from relazione_prodotti_construction as rpco where rpco.id_construction in      (                               select distinct rpc.id_construction from                               (select rpc.id_construction from relazione_prodotti_construction as rpc where rpc.id_prodotto=\''+id_search+'\') as rpc join (select pcc.id_construction from prodotti_construction_contenuti as pcc ) as pcc on rpc.id_construction=pcc.id_construction ) and rpco.id_prodotto!=\''+id_search+'\' ) AS rpco on p.id_prodotto=rpco.id_prodotto GROUP BY p.id_prodotto ORDER BY p.nuovo ASC LIMIT 5';
                                 
                                */
    db.transaction(function(tx) {
                   
                   
                   //tx.executeSql('select * from prodotti where id_prodotto in (select id_prodotto_2 from prodotti_correlati where id_prodotto_1 = '+id_search+')', [], function(tx, results) {
                  /* tx.executeSql('select prod.*,desc.titolo from prodotti as prod join (select distinct id_prodotto,titolo from prodotti_contenuti) as desc on desc.id_prodotto=prod.id_prodotto where prod.id_prodotto in (select id_prodotto_2 from prodotti_correlati where id_prodotto_1 = '+id_search+') and prod.sample=\'no\'', [], function(tx, results) {*/
                   tx.executeSql('select prod.*,desc.titolo from prodotti as prod join (select distinct id_prodotto,titolo from prodotti_contenuti where id_prodotto in (select id_prodotto_2 from prodotti_correlati where id_prodotto_1 = '+id_search+') and id_lingua=\'2\' limit 1) as desc on desc.id_prodotto=prod.id_prodotto  where prod.sample=\'no\'', [], function(tx, results) {   
                                 if (results)
                                 {
                                 var conto=results.rows.length;
                                 if (conto==0)
                                 {
                                 console.log("Checkpoint: inside getInfoProdottiCorrelatiFromDb zero results branch");
                                 var queryC='SELECT * FROM (select * from prodotti as p where p.id_prodotto!=\''+id_search+'\' and p.status=\'si\' and p.sample=\'no\' ) as p join (select distinct rpc.id_prodotto from relazione_prodotti_categorie as rpc where rpc.id_categoria in (SELECT distinct cc.id_categoria FROM relazione_prodotti_categorie AS rpc join categorie_contenuti AS cc on rpc.id_categoria=cc.id_categoria WHERE rpc.id_prodotto=\''+id_search+'\'    ) and rpc.id_prodotto!=\''+id_search+'\' ) AS rpc on p.id_prodotto=rpc.id_prodotto join (select distinct rpco.id_prodotto from relazione_prodotti_construction as rpco where rpco.id_construction in      (           select distinct rpc.id_construction from       (select rpc.id_construction from relazione_prodotti_construction as rpc where rpc.id_prodotto=\''+id_search+'\') as rpc join (select pcc.id_construction from prodotti_construction_contenuti as pcc ) as pcc on rpc.id_construction=pcc.id_construction ) and rpco.id_prodotto!=\''+id_search+'\' ) AS rpco on p.id_prodotto=rpco.id_prodotto join (select distinct id_prodotto,titolo from prodotti_contenuti) as desc on desc.id_prodotto=p.id_prodotto GROUP BY p.id_prodotto ORDER BY p.nuovo ASC LIMIT 5';
                                 tx.executeSql(queryC,[],function(tx,results)
                                               {
                                               callback(results);
                                               }
                                               )
                                 
                                 
                                 
                                 }
                                 else
                                 {
                                 callback(results);
                                 }
                                 }
                                 }, errorDB);
                   
                   },errorDB);
    
}
/*
function getInfoProdottiCorrelatiFromDb(id_search,callback)
{
 
	db.transaction(function(tx) {
 
                   //select * from prodotti join prodotti_correlati on id_prodotto=id_prodotto_2 where id_prodotto_1
                   //'select * from prodotti where id_prodotto in (select id_prodotto_2 from prodotti_correlati where id_prodotto_1 = '+id_search+')'
                   tx.executeSql('select * from prodotti join prodotti_correlati on id_prodotto=id_prodotto_2 where id_prodotto_1 = '+id_search, [], function(tx, results) {
 
 
                                 callback(results);
 
                                 }, errorDB);
                   //diagnostica
                   /*
                    tx.executeSql('select * from prodotti_correlati',[],function(tx,results){
 
 
                    if (results.rows)
                    {console.log("ROW ROW FIGHT THE POWA"+results.rows.length);}
 
                    for (var ind=0; ind<results.rows.length; ind++)
                    {
                    var item=results.rows.item(ind);
                    console.log("id1: "+item.id_prodotto_1+" id2: "+item.id_prodotto_2);
 
                    }
                    });
                    */
            /*
             
                   },errorDB);
    
    
    
    
    
}
*/

/*
 
 function exctractProductLists(results, filters, start, count, callback) {
 if(debug) alert('Products results:' + results.rows.length);
 var allProducts = new Array();
 var limit = Math.min(start + count, results.rows.length);
 for(i=start; i<limit; i++) {
 var item = results.rows.item(i);
 if(debug) alert(results.rows.item(i).categoria_testo);
 allProducts.push(new Product(item.id_prodotto, item.codice_prodotto, item.titolo, null, null, item.id_country, null, item.nuovo, item.immagine, null, null, null, null, null, null, null, null));
 }
 callback(allProducts, results.rows.length);
 }
 */


function exctractProductLists(results, filters, start, count, callback,searchString) {
	/*
     if(debug) alert('Products results:' + results.rows.length);
     var allProducts = new Array();
     var limit = Math.min(start + count, results.rows.length);
     var stringa_download_pdf='';
     var temp_stringa_download_pdf='';
     for(i=start; i<limit; i++) {
     var item = results.rows.item(i);
     if(debug) alert(results.rows.item(i).categoria_testo);
     allProducts.push(new Product(item.id_prodotto, item.codice_prodotto, item.titolo, null, null, item.id_country, null, item.nuovo, item.immagine, null, null, null, null, null, null, null, null));
     temp_stringa_download_pdf+=item.id_prodotto+',';
     }
     
     if (temp_stringa_download_pdf[temp_stringa_download_pdf.length]==',')
     {
     stringa_download_pdf=temp_stringa_download_pdf.substr(0,temp_stringa_download_pdf.length-1);
     }
     
     var contoex=results.rows.length;
     
     db.transaction(function(tx) {
     tx.executeSql('select count(*) as conto from prodotti WHERE prodotti.status = \'si\'', [], function(tx, results) {
     var conte=0;
     var temp=0;
     if (results)
     {
     temp=results.rows.item(0);
     if (temp.conto)
     {
     conte=temp.conto;
     }
     }
     
     callback(allProducts, contoex,stringa_download_pdf,conte);
     
     
     }, errorDB);
     
     },errorDB);
     
     */
    if(debug) alert('Products results:' + results.rows.length);
	var allProducts = new Array();
	var limit = Math.min(start + count, results.rows.length);
	var stringa_download_pdf='';
    var temp_stringa_download_pdf='';
	for(i=start; i<limit; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).categoria_testo);
		allProducts.push(new Product(item.id_prodotto, item.codice_prodotto, item.titolo, null, null, item.id_country, null, item.nuovo, item.immagine, null, null, null, null, null, null, null, null));
		
	}
	var contoex=results.rows.length;
	var filtri=false;
	for(filterIndex in filters) {
        var filter = filters[filterIndex];
        if(filter.filterValues.length > 0)
        {
            filtri=true;
        }
    }
	
	if ((!filtri)&&((searchString=='undefined')||(searchString==null)||(searchString===null)||(searchString==='undefined')))
	{
		stringa_download_pdf='all';
	}
	else
	{
		for (var ind=0; ind<contoex; ind++)
		{
			var item = results.rows.item(ind);
			temp_stringa_download_pdf+=item.id_prodotto+',';
		}
	}
	if (temp_stringa_download_pdf[temp_stringa_download_pdf.length-1]==',')
    {
        stringa_download_pdf=temp_stringa_download_pdf.substr(0,temp_stringa_download_pdf.length-1);
    }
    
    
    //max db table
    
    db.transaction(function(tx) {
                   
                   
                   tx.executeSql('select count(*) as conto from prodotti WHERE prodotti.status = \'si\'', [], function(tx, results) {
                                 var conte=0;
                                 var temp=0;
                                 if (results)
                                 {
                                 temp=results.rows.item(0);
                                 if (temp.conto)
                                 {
                                 conte=temp.conto;
                                 }
                                 }
                                 
                                 callback(allProducts, contoex,stringa_download_pdf,conte);
                                 
                                 
                                 }, errorDB);
                   
                   },errorDB);
    
}


//-------------------------------------------LUCA START
function getProductFromDb(idProduct, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select product full detail');
								 	 var sqlQuery = "SELECT * FROM prodotti " +
												"LEFT JOIN prodotti_contenuti ON prodotti.id_prodotto = prodotti_contenuti.id_prodotto " +
												"LEFT JOIN relazione_prodotti_country ON prodotti.id_prodotto = relazione_prodotti_country.id_prodotto " +
												"WHERE prodotti.id_prodotto=? AND status = 'si' AND (usa = 'si' OR usa = 'no') AND (usa_approvato = 'si' OR usa_approvato = 'no') AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(idProduct, '2'), function(tx, results) { exctractProductDetail(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractProductDetail(results, callback) {
	if(debug) alert('Product detail:' + results.rows.length);
	var allDetails = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).id_prodotto);
		allDetails.push(new Product(results.rows.item(i).id_prodotto, results.rows.item(i).codice_prodotto, results.rows.item(i).titolo, null, null, results.rows.item(i).id_country, null, results.rows.item(i).nuovo, results.rows.item(i).immagine, results.rows.item(i).immagine, results.rows.item(i).descrizione, results.rows.item(i).descrizione_repair, results.rows.item(i).scala_misure, results.rows.item(i).misure, results.rows.item(i).misure_tecniche, results.rows.item(i).dimensioni_foglio, results.rows.item(i).allegato));
	}
	getProductImageFromFileSystem(allDetails[0], callback)
}

function getProductImageFromFileSystem(product, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectoryImages(fileSystem, product, callback);
					}, fail);
}

function resolveDirectoryImages(fileSystem, product, callback) {
	fileSystem.root.getDirectory("VibramCatalogueOffline/images", {create: false, exclusive: false},
							function(parent) {
                                        console.log('dir: ' + parent);
										if(product.imageBig.length > 0) {
											product.imageBig = parent.toURI() + "/" + product.imageBig;
										}
										else {
											product.imageBig = "";
										}
										
										//alert(product.image);
										resolveDirectorySmallImagesForAttachment(fileSystem, product, callback)
							}, fail);
}


//function getProductImageSmallFromFileSystem(product, callback) {
//	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
//						resolveDirectorySmallImagesForAttachment(fileSystem, product, callback);
//					}, fail);
//}

function resolveDirectorySmallImagesForAttachment(fileSystem, product, callback) {
	console.log('resolveDirectorySmallImages')
	fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: false, exclusive: false},
							function(parent) {
                                        console.log(parent)
										if(product.image.length > 0) {
											product.image = parent.toURI() + "/" + product.image;
										}
										else {
											product.image = "";
										}
										//product.image = "/Documents/images_small/" + product.image;
										//alert(product.image);
										getProductAttachmentFromFileSystem(product, callback);
							}, fail);
}



function getProductAttachmentFromFileSystem(product, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectoryAttachment(fileSystem, product, callback);
					}, fail);
}

function resolveDirectoryAttachment(fileSystem, product, callback) {
	fileSystem.root.getDirectory("VibramCatalogueOffline/pdf", {create: false, exclusive: false},
							function(parent) {
										if(product.attach.length > 0) {
											product.attach = parent.toURI() + "/" + product.attach;
											console.log('indirizzo allegato: ' + product.attach);
										}
										else {
											product.attach = "";
										}
										//product.image = "/Documents/images_small/" + product.image;
										//alert(product.image);
										callback(product);
							}, fail);
}

function getColorsFromDb(product, callback) {
	if(debug) alert("Function getColors called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select colors');
								 tx.executeSql('SELECT * FROM prodotti_colori JOIN prodotti_colori_contenuti ON prodotti_colori.id_colori = prodotti_colori_contenuti.id_colori JOIN relazione_prodotti_colori ON prodotti_colori.id_colori=relazione_prodotti_colori.id_colori  WHERE id_lingua = ? AND  relazione_prodotti_colori.id_prodotto = ?', Array('2',product.id), function(tx, results) { exctractColors(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function exctractColors(results, product, callback) {
	if(debug) alert('Colors results:' + results.rows.length);
	var allColors = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).codice_colore);
		allColors.push(new Color(results.rows.item(i).id_colori, results.rows.item(i).codice_colore, results.rows.item(i).testo_colori));
	}
	callback(allColors);
}

function getRelatedAttachmentsFromDb(product, callback) {
	if(debug) alert("Function getColors called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select attachment');
								 tx.executeSql('SELECT * FROM prodotti_allegati WHERE status = ? AND  id_prodotto = ?', Array('si',product.id), function(tx, results) { exctractRelatedAttachments(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function exctractRelatedAttachments(results, product, callback) {
	if(debug) alert('Attachments results:' + results.rows.length);
	var allAttachments = new Array();
	console.log("qui prodotto " + product)
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		allAttachments.push(new Attachment(item.id_allegati, item.nome_allegato, item.tipo_allegato, item.estensione, null, null));
	}
	callback(allAttachments, product);
}

function getAttachmentImageDetailsFromDb(attachment, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectoryAttachmentSmallImage(fileSystem, attachment, callback);
						
					}, fail);
}

function resolveDirectoryAttachmentSmallImage(fileSystem, attachment, callback) {
	fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: false, exclusive: false},
							function(parent) {
										if(attachment.attachmentName.length > 0) {
											attachment.imageSmall = parent.toURI() + "/" + attachment.attachmentName;
										}
										else {
											attachment.imageSmall = "";
										}
										resolveDirectoryAttachmentBigImage(fileSystem, attachment, callback);
										//callback(attachment)
							}, fail);
}

function resolveDirectoryAttachmentBigImage(fileSystem, attachment, callback) {
	fileSystem.root.getDirectory("VibramCatalogueOffline/images", {create: false, exclusive: false},
							function(parent) {
										if(attachment.attachmentName.length > 0) {
											attachment.imageBig = parent.toURI() + "/" + attachment.attachmentName;
										}
										else {
											attachment.imageBig = "";
										}
										callback(attachment);
							}, fail);
}


function getProductImageSmallFromFileSystem_detail(id, img_path,callback) {
    //console.log("Prima di request file system");
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                             /*
                              
                              if (fileSystem)
                              {console.log(fileSystem);}
                              if (resolveDirectorySmallImages_detail)
                              {console.log(resolveDirectorySmallImages_detail);}
                              if ((callback=='undefined')||(callback==='undefined'))
                              {console.log("callback undefined");}
                              else
                              {console.log("callback not undefined: "+callback);}
                              if (img_path)
                              {console.log(img_path);}
                              if (id)
                              {console.log(id);}
                              */
                             resolveDirectorySmallImages_detail(fileSystem,id, img_path, callback);
                             /*
                              console.log("Prima di root get directory");
                              fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: false, exclusive: false},
                              function(parent) {
                              
                              if ((callback=='undefined')||(callback==='undefined'))
                              {console.log("callback undefined");}
                              else
                              {console.log("callback not undefined: "+callback);}
                              
                              var path='';
                              if(img_path.length > 0) {
                              path = parent.toURL() + "/" + img_path;
                              } else {
                              path = "img/immagine1.jpg";
                              }
                              //product.image = "/Documents/images_small/" + product.image;
                              //alert(product.image);
                              
                              
                              callback(id,path);
                              }, fail);
                              
                              */
                             }, fail );
}
function resolveDirectorySmallImages_detail(fileSystem,id, image, callback) {
    //console.log("Prima di root get directory");
	fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: false, exclusive: false},
                                 function(parent) {
                                 var path='';
                                 if(image.length > 0) {
                                 path = parent.toURI() + "/" + image;
                                 } else {
                                 path = "img/immagine1.jpg";
                                 }
                                 //product.image = "/Documents/images_small/" + product.image;
                                 //alert(product.image);
                                 callback(id,path);
                                 }, fail);
}



//-------------------------------------------LUCA END

function getProductShortDetailFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select product short detail');
								 var sqlQuery = "SELECT categoria_testo FROM relazione_prodotti_categorie JOIN categorie_contenuti ON relazione_prodotti_categorie.id_categoria = categorie_contenuti.id_categoria WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCategoryProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCategoryProduct(results, product, callback) {
	if(debug) alert('Product categories results:' + results.rows.length);
	var allCategories = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.categoria_testo);
		allCategories.push(item.categoria_testo);
	}
	product.categories = allCategories;
	getProductCountriesFromDb(product, callback);
}

function getProductCountriesFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select countries short detail');
								 var sqlQuery = "SELECT testo_country FROM relazione_prodotti_country JOIN prodotti_country_contenuti ON relazione_prodotti_country.id_country = prodotti_country_contenuti.id_country WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCountryProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCountryProduct(results, product, callback) {
	if(debug) alert('Product countries results:' + results.rows.length);
	var allCountries = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_country);
		allCountries.push(item.testo_country);
	}
	product.countries = allCountries;
	getProductCompoundsFromDb(product, callback);
}

function getProductCompoundsFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select compounds short detail');
								 var sqlQuery = "SELECT testo_compound FROM relazione_prodotti_compound JOIN prodotti_compound_contenuti ON relazione_prodotti_compound.id_compound = prodotti_compound_contenuti.id_compound WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCompoundProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCompoundProduct(results, product, callback) {
	if(debug) alert('Product compounds results:' + results.rows.length);
	var allCompounds = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_compound);
		allCompounds.push(item.testo_compound);
	}
	product.compounds = allCompounds;
	getProductConstructionsFromDb(product, callback);
}

function getProductConstructionsFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select constructions short detail');
								 var sqlQuery = "SELECT testo_construction FROM relazione_prodotti_construction JOIN prodotti_construction_contenuti ON relazione_prodotti_construction.id_construction = prodotti_construction_contenuti.id_construction WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultConstructionProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultConstructionProduct(results, product, callback) {
	if(debug) alert('Product contruction results:' + results.rows.length);
	var allConstructions = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_construction);
		allConstructions.push(item.testo_construction);
	}
	product.constructions = allConstructions;
	getProductMescoleFromDb(product, callback);
}

function getProductMescoleFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select mescole short detail');
								 var sqlQuery = "SELECT testo_mescole FROM relazione_prodotti_mescole JOIN prodotti_mescole_contenuti ON relazione_prodotti_mescole.id_mescole = prodotti_mescole_contenuti.id_mescole WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultMescoleProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultMescoleProduct(results, product, callback) {
	if(debug) alert('Product mescole results:' + results.rows.length);
	var allMescole = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_mescole);
		allMescole.push(item.testo_mescole);
	}
	product.mescole = allMescole;
	getProductImageSmallFromFileSystem(product, callback);
}

function getProductImageSmallFromFileSystem(product, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectorySmallImages(fileSystem, product, callback);
					}, fail);
}


function resolveDirectorySmallImages(fileSystem, product, callback) {
	fileSystem.root.getDirectory("VibramCatalogueOffline/images_small", {create: false, exclusive: false},
							function(parent) {
										if(product.image.length > 0) {
											product.image = parent.toURI() + "/" + product.image;
										} else {
											product.image = "";
										}
										//product.image = "/Documents/images_small/" + product.image;
										//alert(product.image);
										callback(product);
							}, fail);
}

function getSyncFromDb(callback) {
	updates = 0;
	OpenDb();
	db.transaction(function(tx) {
								 console.log('select sync short detail');
								 var sqlQuery = "SELECT * FROM synchronization ORDER BY maxDate DESC";
								 tx.executeSql(sqlQuery,[], function(tx, results) { resultSyncFromDb(results, callback); }, errorDB);
								},
				   errorDB);
}

function resultSyncFromDb(results, callback) {
//	synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)
if(debug) alert('Sync results:' + results.rows.length);
	var allSync = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		allSync.push(new Sync(item.id, item.date, item.addedProducts, item.updatedProducts, item.maxDate));
	}
	if(allSync.length > 0) {
		lastSyncData = allSync[0].maxDate;
	}
	callback(allSync);
}

function lastSyncFromDb(results, callback) {
//	synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)
if(debug) alert('Sync results:' + results.rows.length);
	var allSync = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		allSync.push(new Sync(item.id, item.date, item.addedProducts, item.updatedProducts, item.maxDate));
	}
	if(allSync.length > 0) {
		lastSyncData = allSync[0].maxDate;
	}
	callback(allSync);
}



//brand!
function getBrandFromDb(callback) {
	OpenDb();
	db.transaction(function(tx) {
								 console.log('select brand');
								 var sqlQuery = "SELECT * FROM prodotti_brand ORDER BY nome_brand ASC";
								 console.log("query "+sqlQuery)
								 tx.executeSql(sqlQuery,[], function(tx, results) { resultBrandFromDb(results, callback); }, errorDB);
								},
				   errorDB);
}

function resultBrandFromDb(results, callback) {
//	synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)
if(debug) alert('Brand results:' + results.rows.length);
	console.log("chiamo result brand >> " + results);
	console.log("quanti sono >> " + results.rows.length);
	var allBrand = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		console.log("ciclo... >> " + item.id_brand)
		allBrand.push(new Brand(item.id_brand, item.nome_brand, item.indirizzo_brand));
	}
	callback(allBrand);
}

//array repair
function generoArrayRepairFromDb() {
	console.log("entro in generoArrayRepairFromDb")
	OpenDb();
	db.transaction(function(tx) {
								 console.log('select repair array');
								 var sqlQuery = "SELECT * FROM categorie WHERE id_livello1='4'";
								 console.log("query "+sqlQuery)
								 tx.executeSql(sqlQuery,[], function(tx, results) { resultArrayRepairFromDb(results); }, errorDB);
								},
				   errorDB);
}
function resultArrayRepairFromDb(results) {
	console.log("lunghe "+results.rows.length)
	for(i=0; i<results.rows.length; i++) {
		console.log("quantooo " + i)
		var item = results.rows.item(i);
		array_repair.push(item.id_categorie);
	}
	console.log(array_repair.length)
}
// Transaction error callback
function errorDB(tx, err) {
	alert("tx error code: " + tx.code);
	alert("tx error message: " + tx.message);
	if(err) {
		alert("Error processing SQL: " + err.message);
	}
}

function fail(e) {
	var msg = '';

  switch (e.code) {
    case FileError.QUOTA_EXCEEDED_ERR:
      msg = 'QUOTA_EXCEEDED_ERR';
      break;
    case FileError.NOT_FOUND_ERR:
      msg = 'NOT_FOUND_ERR';
      break;
    case FileError.SECURITY_ERR:
      msg = 'SECURITY_ERR';
      break;
    case FileError.INVALID_MODIFICATION_ERR:
      msg = 'INVALID_MODIFICATION_ERR';
      break;
    case FileError.INVALID_STATE_ERR:
      msg = 'INVALID_STATE_ERR';
      break;
    default:
      msg = 'Unknown Error';
      break;
  };

  alert('Error: ' + msg);
}
