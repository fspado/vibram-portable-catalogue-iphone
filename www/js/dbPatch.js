var UpdateStrings=new Array();

UpdateStrings.push(new DbPatch('1.0','1.1','ALTER TABLE prodotti ADD COLUMN sample'));
UpdateStrings.push(new DbPatch('1.0','1.1','CREATE TABLE IF NOT EXISTS prodotti_correlati (id_prodotto_1 int, id_prodotto_2 int, data_modifica)'));
UpdateStrings.push(new DbPatch('1.0','1.2','ALTER TABLE prodotti ADD COLUMN eumarketonly'));
UpdateStrings.push(new DbPatch('1.1','1.1','ALTER TABLE prodotti ADD COLUMN eumarketonly'));


function updateDb(db,from,to,callback,cbkparams)
{
	var stringheUpdate=new Array();
	
	for (var index in UpdateStrings)
	{
		var obj=UpdateStrings[index];
		if (obj.updateString)
		{
			if ((obj.fromVersion==from)&&(obj.toVersion==to))
				{
					console.log(obj.updateString);
					stringheUpdate.push(obj.updateString);
				}
		}
	}
	if (stringheUpdate.length>0)
	{
		console.log(stringheUpdate.length+' patch actions');
		db.transaction(function(tx){
		
			console.log('Applying patch');
			applyPatch(tx,stringheUpdate,to);
			
			
		},errorDB,function(){
			
			callback(cbkparams);
			
		});
	}
	else
	{
		console.log('No patch actions');
		callback(cbkparams);
	}
	
}


function applyPatch(tx,stringheUpdate,toVersion)
{
	for (var ind in stringheUpdate)
	{
		tx.executeSql(stringheUpdate[ind]);
		console.log(stringheUpdate);
		
	}
	
	var stringUpdateVer="update existance set Version='"+toVersion+"'";
	tx.executeSql(stringUpdateVer);
}