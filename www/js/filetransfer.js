function FileTransferForDesktop(parent)
{
	this.attempt = 0;
	var that = this;
	var timer;
	var TIMEOUT = 10000;
	
	this.download = function (source, target, saveImageSuccess, saveImageError, fileName)
	{
		console.log('function download');
		var xhr = new XMLHttpRequest();
		console.log('Object XHR created');
		xhr.open('GET', source, true);
		xhr.responseType = 'arraybuffer';
		timer = setTimeout(function () {     /* vs. a.timeout */
			handleTimeOut(xhr, source, that.attempt);
		}, TIMEOUT);            /* vs. a.ontimeout */
		
		xhr.onabort = function() {to(that, source, target, saveImageSuccess, saveImageError, fileName);};  
		//xhr.ontimeout = function(saveImageError)
		//{
		//	console.log('Timeout, file ' + fileName + ' not downloaded');
		//	saveImageError();
		//}

		xhr.onload = function(e) {
		  clearTimeout(timer);
		  console.log('Onload ' + source);
		  if (this.status == 200) {
			BlobBuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder;
			var bb = new BlobBuilder();
			bb.append(this.response); // Note: not xhr.responseText

			var blob = bb.getBlob();
			dataStore(fileName, parent, blob, saveImageSuccess, saveImageError);
		  }
		  else
		  {
			console.log('Impossible to download file ' + fileName + ': status is ' + this.status);
			saveImageError();
		  }
		};
		xhr.onerror = function(e)
		{
			clearTimeout(timer);
			console.log('Impossible to download file ' + fileName + '. HtmlHttpError');
			saveImageError();
		}

		console.log('Before send ' + source);
		xhr.send();
		console.log('After send ' + source);
	}

}

function handleTimeOut(xhr, source, attempt) {
	xhrState = xhr.readyState;
		console.log('timeout fired for the ' + attempt + ' time for source: ' + source + ' Xhr state: ' + xhrState);
		if (xhrState < 4) {
			xhr.abort();
		}
}

function to(fileTransferForDesktop, source, target, saveImageSuccess, saveImageError, fileName, attempt) {
		console.log('Timeout ' + source);
		fileTransferForDesktop.attempt++;
		if( fileTransferForDesktop.attempt < 3) {
			fileTransferForDesktop.download(source, target, saveImageSuccess, saveImageError, fileName);
		}
		else {
			console.log("Request Timeout\nFailed to access " + source);
			saveImageError();
		}
	};

function dataStore(fileName, parent, blob, saveImageSuccess, saveImageError)
{
	console.log('function dataStore');
	parent.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) {
		console.log('Downloading ' + parent.fullPath + fileName + '...');
		fileEntry.createWriter(function(writer) {  // FileWriter
		
			writer.onwrite = function(e) {
				console.log('File '+ parent.fullPath + fileName + ' succesfully writed.');
			};
		
			writer.onerror = function(e) {
			  console.log('Write failed: ' + e);
			  saveImageError();
			};
		
			writer.write(blob);
			saveImageSuccess(fileEntry);
		
		},
		function(err){
			console.log(err.code);
			saveImageError(err);
		}
		);
	});
}