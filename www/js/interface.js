window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
var version = new Version();
console.log('Version ' + version.value);
if(version.value == "Chrome")
{
	var FileTransfer = FileTransferForDesktop;
}
// JavaScript Document

// Returns a User object or null if the user table is empty
// PARAMETERS: callback:function -> the function that will be called with the User object or null as parameter
function v_start(callback) {
	checkDbExistance(callback);
}

// Returns an activation state string
// PARAMETERS: username:string -> the user username
//			   password:string -> the user password
//			   callback:function -> the function that will be called with the activation state of the user and the userId as parameter
function v_login(username, password, callback) {
	checkAuthentication(username, password, callback);
}

// Returns a User object
// PARAMETERS: hash:string -> an hash of username and password
//			   password:string -> the user password
//			   callback:function -> the function that will be called with the activation state of the user as parameter
function getUser(hash, callback) {
	getUserFromDb(hash, callback);
}

function synchronize(userId, callback) {
	synchronizeDb(userId, callback);
}

function getUpdates(callback){
	getLastSyncToCheckUpdates(callback);
}

// Returns an array of Category objects
// PARAMETERS: callback:function -> the function that will be called with the array of categories as parameter
function getCategories(callback) {
	getCategoriesFromDb(callback);
}

// Returns an array of Category objects
// PARAMETERS: callback:function -> the function that will be called with the array of categories as parameter
function getBrands(callback) {
	getBrandsFromDb(callback);
}

// Returns an array of Compound objects
// PARAMETERS: callback:function -> the function that will be called with the array of compounds as parameter
function getCompounds(callback) {
	getCompoundsFromDb(callback);
}

// Returns an array of Country objects
// PARAMETERS: callback:function -> the function that will be called with the array of countries as parameter
function getCountries(callback) {
	getCountriesFromDb(callback);
}

// Returns an array of Construction objects
// PARAMETERS: callback:function -> the function that will be called with the array of constructions as parameter
function getConstructions(callback) {
	getConstructionsFromDb(callback);
}

// Returns an array of Product objects
// PARAMETERS: filters:function -> an array of Filter objects
//			   sortOrder:enum{id_prodotto, codice_prodotto, titolo} -> one of the possible sorting criteria
//			   order:enum{desc, asc} -> one of the possible sorting order
//			   start:int -> the value to start from to get a the list of products
//			   count:int -> the maximum number of products to retrieve
//			   exclusive:bool -> true for exclusive product catalogue, false otherwise
//			   searchString:string -> a text to be searched in product code and name
//			   callback:function -> the function that will be called with the array of Product as parameter
function getProductList(filters, sortOrder, order, start, count, exclusive, searchString, callback) {
	getProductListFromDb(filters, sortOrder, order, start, count, exclusive, searchString, callback);
}

// Returns a Product object completed with all details
// PARAMETERS: product:Product object -> the product object of wich you want short details
//			   callback:function -> the function that will be called with the Product as parameter
function getProductShortDetail(product, callback) {
	getProductShortDetailFromDb(product, callback) 
}

// Returns an array of Colors objects
// PARAMETERS: callback:function -> the function that will be called with the array of colors as parameter
function getColors(product, callback) {
	getColorsFromDb(product, callback);
}

// Returns an array of Attachments objects
// PARAMETERS: callback:function -> the function that will be called with the array of Attachments as parameter
function getRelatedAttachments(product, callback) {
	getRelatedAttachmentsFromDb(product, callback);
}

// Returns an Attachment object
// PARAMETERS: attachment:Attachment object with an image
//			   callback:function -> the function that will be called with the array of Attachments as parameter
function getAttachmentImageDetails(attachment, callback) {
	
	getAttachmentImageDetailsFromDb(attachment, callback);
}

// Returns an Attachment object
// PARAMETERS: attachment:Attachment object with a pdf file
//			   callback:function -> the function that will be called with the array of Attachments as parameter
function getAttachmentPdfDetails(attachment, callback) {
	getAttachmentPdfDetailsFromDb(attachment, callback);
}

// Returns a Product object completed with all details
// PARAMETERS: product:Product object -> the product object of wich you want all details
//			   callback:function -> the function that will be called with the Product as parameter
function getProduct(idProduct, callback) {
	getProductFromDb(idProduct, callback) 
}

// Returns a syncro object with all details.
// PARAMETERS: syncros: synchronization history object -> the synchronization object with all details
//			   callback: function -> the function that will be called with the Synchronization History as parameter 
function getSynchronizationHistory(callback){
	getSynchronizationHistoryFromDb(callback);
}

// Returns an array of Sync objects
// PARAMETERS: callback:function -> the function that will be called with the array of Sync objects as parameter
function getSync(callback) {
	getSyncFromDb(callback);
}

// Returns an array of Brand objects
// PARAMETERS: callback:function -> the function that will be called with the array of Brand objects as parameter
function getBrand(callback) {
	getBrandFromDb(callback);
}

// Returns an array of Brand objects
// PARAMETERS: callback:function -> the function that will be called with the array of Brand objects as parameter
function generoArrayRepair() {
	generoArrayRepairFromDb();
}

function changeUser(callback)
{
	deleteUserFromDb(callback);
}


function getInfoProdottiCorrelati(id,callback)
{
	getInfoProdottiCorrelatiFromDb(id,callback);
}

function populateImgPathFromLocalFileSystem(id,path,callback)
{
    /*
     console.log("prima di errore?");
     if (callback=='undefined')
     {console.log("callback undefined");}
     else
     {console.log("callback not undefined: "+callback);}
     */
	getProductImageSmallFromFileSystem_detail(id,path,callback);
}
